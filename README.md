# README #

##ZohoBooks API Library##

* Javascript Library for easy access to ZohoBooks API.
* Built for node/io applications.  
* No version yet.  Still completing basics.

This library is being built to clean up our internal usage of the ZohoBooks API, as we use it for integration with Quotewerks.  

## Dependencies ##

* Requires [request](https://github.com/request/request) module

## Usage ##

### Basic Concept ###

`ZohoBooks.ListContacts(function(data){
   //parse data
})`

###Node Example###

```
var zohobooks = require('zohobooks');

var MyBooks = new zohobooks('HO3LBl1SvI4KlgV3IthZfyINmLYwL70t'); // zoho authtoken

router.get('/', function (req, res, next) {

    MyBooks.ListContacts(function(body){
        res.send(body);
    });
    
});
```

### Contributors ###

* [Michael Kozak](mailto:dev@meteormanaged), for [Meteor Managed](http://www.meteormanaged.com)